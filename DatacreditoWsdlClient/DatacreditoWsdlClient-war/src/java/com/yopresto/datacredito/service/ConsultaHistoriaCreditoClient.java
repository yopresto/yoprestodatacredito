/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yopresto.datacredito.service;

import com.yopresto.datacredito.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;

/**
 *
 * @author waar1
 */
public class ConsultaHistoriaCreditoClient {

    @org.junit.Test
    public void testWsService() throws HC2PNJException_Exception {
        
        try{
            //Configuracion de certificados SSL POINT-TO-POINT   
            System.setProperty("javax.net.ssl.trustStore", "D:\\datacredito.truststore.jks");
            System.setProperty("javax.net.ssl.trustStorePassword", "Y0Pr3st0");
            System.setProperty("javax.net.ssl.keyStore", "D:\\datacredito.truststore.jks");
            System.setProperty("javax.net.ssl.keyStorePassword", "Y0Pr3st0");
            
            JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
            factory.setAddress("https://demo-servicesesb.datacredito.com.co/wss/dhws3/services/DHServicePlus?wsdl");
            factory.setServiceClass(HC2PNJService.class);
            Object client = factory.create();
            LoggingOutInterceptor loggingOutInterceptor = new LoggingOutInterceptor();
            loggingOutInterceptor.setPrettyLogging(true);
            ClientProxy.getClient(client).getOutInterceptors().add(loggingOutInterceptor);
            LoggingInInterceptor loggingInInterceptor = new LoggingInInterceptor();
            loggingInInterceptor.setPrettyLogging(true);
            ClientProxy.getClient(client).getInInterceptors().add(loggingInInterceptor);

            //Propiedades de seguridad UsernameToken +  Timestamp + Signature         
            Map<String, Object> propsOut = new HashMap<>();
            propsOut.put(WSHandlerConstants.USER, "2-901095036");
            /*propsOut.put(WSHandlerConstants.TIMESTAMP, "60");
            propsOut.put(WSHandlerConstants.USERNAME_TOKEN, "Y0Pr3st02019*");*/
            propsOut.put(WSHandlerConstants.SIGNATURE, "3f63d2108f75e79b4216e3de01593f8c18525031");
            propsOut.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN + " " + WSHandlerConstants.TIMESTAMP + " " + WSHandlerConstants.SIGNATURE);
            propsOut.put(WSHandlerConstants.SIG_PROP_FILE, "D:\\crypto.properties");
            propsOut.put(WSHandlerConstants.ADD_UT_ELEMENTS, WSConstants.NONCE_LN + " " + WSConstants.CREATED_LN);
            propsOut.put(WSHandlerConstants.MUST_UNDERSTAND, "false");
            propsOut.put(WSHandlerConstants.SIGNATURE_PARTS, "{Content}{http://schemas.xmlsoap.org/soap/envelope/}Body;{Element}{http://docs.oasisopen.org/wss/2004/01/oasis-200401-wss-wssecurity-secext1.0.xsd}UsernameToken;{Element}{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurityutility-1.0.xsd}Timestamp");
            propsOut.put(WSHandlerConstants.SIG_KEY_ID, "DirectReference");
            propsOut.put(WSHandlerConstants.SIG_ALGO, "http//www.w3.org/2000/09/xmldsig#rsa-sha1");
            propsOut.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
            //propsOut.put(WSHandlerConstants.PW_CALLBACK_CLASS, SignaturePwdClientCallBackHandler.class.getName();                 
            WSS4JOutInterceptor wss4jOut = new WSS4JOutInterceptor(propsOut);
            ClientProxy.getClient(client).getOutInterceptors().add(wss4jOut);          

            /*https URL hostname does not match the Common Name (CN) on the server certificate in the client's truststore.  Make sure server certificate          is correct, or to disable this check (NOT recommended for production) set the CXF client TLS configuration property "disableCNCheck" to true.*/
            HTTPConduit httpConduit = (HTTPConduit) ClientProxy.getClient(client).getConduit();
            TLSClientParameters tlsCP = new TLSClientParameters();
            tlsCP.setDisableCNCheck(true);
            httpConduit.setTlsClientParameters(tlsCP);

            HC2PNJService port = ((HC2PNJService) client);
            System.out.println("Invoking consultarHC2PJ...");

            SolicitudPlus _consultarHC2PJ_solicitud = new SolicitudPlus();
            _consultarHC2PJ_solicitud.setClave("18NVE");
            _consultarHC2PJ_solicitud.setIdentificacion("610967240");
            _consultarHC2PJ_solicitud.setPrimerApellido("Tenjo");
            _consultarHC2PJ_solicitud.setProducto("60");
            _consultarHC2PJ_solicitud.setTipoIdentificacion("2");
            _consultarHC2PJ_solicitud.setUsuario("1072647634");
                        
            System.out.println("Calling");
            System.out.println(_consultarHC2PJ_solicitud.toString());
            String _consultarHC2PJ__return = port.consultarHC2PJ(_consultarHC2PJ_solicitud);
            System.out.println("consultarHC2PJ.result=" + _consultarHC2PJ__return);
        }catch(Exception ex){
            ex.printStackTrace();
        }


    }
    
}
